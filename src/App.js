import React, { Component } from 'react'
import Header from './components/Header'
import Table from './components/Table'
import SubHeader from './components/SubHeader'
import BookDetailModal from './components/BookDetailModal'
import Pagination from './components/Pagination'
import { Open } from './request'

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      amount: 0,
      books: [],
      currentPage: 1,
      amountPerPage: 10,
      filter: '',
      initialYear: '',
      endYear: '',
      bookDetail: null,
      openDoalog: false,
      totalPages: 0,
    }

    this.searchBook = this.searchBook.bind(this)
    this.onInputChange = this.onInputChange.bind(this)
    this.onInitialYearInputChange = this.onInitialYearInputChange.bind(this)
    this.onEndYearInputChange = this.onEndYearInputChange.bind(this)
    this.onDetailButton = this.onDetailButton.bind(this)
    this.onDetailButtonClose = this.onDetailButtonClose.bind(this)
    this.goToPage = this.goToPage.bind(this)
  }

  componentWillMount() {
    this.searchBook()
  }

  async getBooks() {
    try {
      const ret = await Open.getBooks(this.state.currentPage, this.state.amountPerPage, {
        filter: this.state.filter,
        initialYear: this.state.initialYear,
        endYear: this.state.endYear,
      })
      if (!ret.success) return console.error('TCL: App -> getBooks -> ret.data', ret.data)
      this.setState({
        books: ret.data,
      })
    } catch (error) {
      console.error('TCL: App -> getBooks -> error', error)
    }
  }

  async getAmount() {
    try {
      const ret = await Open.getAmount({
        filter: this.state.filter,
        initialYear: this.state.initialYear,
        endYear: this.state.endYear,
      })
      if (!ret.success) return console.error('TCL: Table -> getAmount -> ret.data', ret.data)
      this.setState({
        amount: ret.data,
        totalPages: ret.data / this.state.amountPerPage,
        openDoalog: true,
      })
    } catch (error) {
      console.error('TCL: Table -> getAmount -> error', error)
    }
  }

  searchBook() {
    this.setState({
      currentPage: 1,
    })
    setTimeout(() => {
      this.getAmount()
      this.getBooks()
    }, 0)
  }

  onInputChange(e) {
    this.setState({
      filter: e.target.value,
    })
  }

  onInitialYearInputChange(e) {
    this.setState({
      initialYear: e.target.value,
    })
  }

  onEndYearInputChange(e) {
    this.setState({
      endYear: e.target.value,
    })
  }

  async onDetailButton(bookId) {
    try {
      const ret = await Open.getBook(bookId)
      if (!ret.success) return console.log('TCL: App -> onDetailButton -> error', ret.data)
      this.setState({
        bookDetail: ret.data,
        modalShow: 'show',
        display: 'block',
      })
    } catch (error) {
      console.log('TCL: App -> onDetailButton -> error', error)
    }
  }

  onDetailButtonClose() {
    this.setState({
      bookDetail: null,
      modalShow: '',
      display: 'none',
    })
  }

  goToPage(page) {
    switch (page) {
      case 'previous':
        if (this.state.currentPage === 1) return
        this.setState({
          currentPage: this.state.currentPage - 1,
        })
        break
      case 'next':
        if (this.state.currentPage === this.state.totalPages) return
        this.setState({
          currentPage: this.state.currentPage + 1,
        })
        break
      default:
        this.setState({
          currentPage: page,
        })
    }
    this.getBooks()
  }

  render() {
    return (
      <div className="container-fluid" style={{ marginTop: '10px' }}>
        <BookDetailModal
          bookDetail={this.state.bookDetail}
          openDoalog={this.state.openDoalog}
          modalShow={this.state.modalShow}
          display={this.state.display}
          onDetailButtonClose={this.onDetailButtonClose}
        />
        <div className="row no-gutters">
          <div className="col-12">
            <Header
              searchBook={this.searchBook}
              inputValue={this.state.filter}
              onInputChange={this.onInputChange}
            />
          </div>
          <div className="col-12" style={{ marginBottom: '10px' }}>
            <SubHeader
              amount={this.state.amount}
              onInitialYearInputChange={this.onInitialYearInputChange}
              initialYear={this.state.initialYear}
              onEndYearInputChange={this.onEndYearInputChange}
              endYear={this.state.endYear}
            />
          </div>
          <div className="col-12">
            <Table books={this.state.books} onDetailButton={this.onDetailButton} />
          </div>
          <div className="col-12">
            <Pagination
              currentPage={this.state.currentPage}
              totalPages={this.state.totalPages}
              goToPage={this.goToPage}
            />
          </div>
        </div>
      </div>
    )
  }
}
