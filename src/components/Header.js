import React from 'react'

export default class Header extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      inputValue: '',
    }

    this.onSearchButton = this.onSearchButton.bind(this)
    this.onInputChange = this.onInputChange.bind(this)
  }

  onSearchButton() {
    this.props.searchBook(this.state.inputValue)
  }

  onInputChange(e) {
    this.props.onInputChange(e)
  }

  render() {
    return (
      <div className="row">
        <div className="col-2">
          <p className="navbar-brand"> Supero </p>
        </div>
        <div className="col-8">
          <input
            className="form-control mr-sm-2"
            type="text"
            placeholder="Buscar livro pelo titulo, autor ou ISBN"
            value={this.props.inputValue}
            onChange={this.onInputChange}
          />
        </div>
        <div className="col-2">
          <button className="btn btn-info my-2 my-sm-0" onClick={this.onSearchButton}>
            Buscar
          </button>
        </div>
      </div>
    )
  }
}
