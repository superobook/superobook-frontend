import React, { Component } from 'react'

class Table extends Component {
  constructor(props) {
    super(props)

    this.handleTableBody = this.handleTableBody.bind(this)
  }

  onDetailButton(bookId) {
    this.props.onDetailButton(bookId)
  }

  handleTableBody() {
    if (this.props.books && this.props.books.length) {
      return this.props.books.map((book) => {
        return (
          <tr key={book.id}>
            <th>{book.title}</th>
            <td>{book.author}</td>
            <td>{book.publishingCompany}</td>
            <td>{book.year}</td>
            <td>
              <button
                type="button"
                className="btn btn-outline-info"
                onClick={this.onDetailButton.bind(this, book.id)}
              >
                <i className="fa fa-eye" style={{ fontSize: '20px' }} />
              </button>
            </td>
          </tr>
        )
      })
    }
    return []
  }

  render() {
    return (
      <table className="table table-hover">
        <thead>
          <tr>
            <th scope="col">Livro</th>
            <th scope="col">Autor</th>
            <th scope="col">Editora</th>
            <th scope="col">Ano</th>
            <th scope="col">Ação</th>
          </tr>
        </thead>
        <tbody>{this.handleTableBody()}</tbody>
      </table>
    )
  }
}

export default Table
