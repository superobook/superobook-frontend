import React, { Component } from 'react'

class Pagination extends Component {
  constructor(props) {
    super(props)

    this.mountPages = this.mountPages.bind(this)
  }

  goToPage(page) {
    this.props.goToPage && this.props.goToPage(page)
  }

  mountPages() {
    const pages = this.props.totalPages
    let pagesEl = []
    for (let i = 0; i < pages; i++) {
      let page = i + 1
      pagesEl.push(
        <li className="page-item" key={page}>
          <button
            className="page-link active"
            style={{ backgroundColor: this.props.currentPage === page ? 'lightgray' : '' }}
            onClick={this.goToPage.bind(this, page)}
          >
            {page}
          </button>
        </li>
      )
    }
    return pagesEl
  }

  render() {
    return (
      <div>
        <nav aria-label="Page navigation example">
          <ul className="pagination justify-content-center">
            <li className="page-item">
              <button className="page-link" onClick={this.goToPage.bind(this, 'previous')}>
                Anterior
              </button>
            </li>
            {this.mountPages()}
            <li className="page-item">
              <button className="page-link" onClick={this.goToPage.bind(this, 'next')}>
                Próximo
              </button>
            </li>
          </ul>
        </nav>
      </div>
    )
  }
}

export default Pagination
