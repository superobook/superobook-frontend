import React, { Component } from 'react'

class SubHeader extends Component {
  constructor(props) {
    super(props)

    this.onInitialYearInputChange = this.onInitialYearInputChange.bind(this)
    this.onEndYearInputChange = this.onEndYearInputChange.bind(this)
  }

  onInitialYearInputChange(e) {
    this.props.onInitialYearInputChange(e)
  }

  onEndYearInputChange(e) {
    this.props.onEndYearInputChange(e)
  }

  render() {
    return (
      <div className="row">
        <div className="col-9">
          <div className="row">
            <label className="col-4 col-form-label"> Filtrar ano de publicação: </label>
            <div className="col">
              <div className="row">
                <div className="col-4">
                  <div className="row">
                    <div className="col-9">
                      <input
                        type="number"
                        min="1500"
                        className="form-control"
                        placeholder="Ano"
                        value={this.props.initialYear}
                        onChange={this.onInitialYearInputChange}
                      />
                    </div>
                    <div className="col-3" style={{ paddingTop: '7px' }}>
                      <i className="fa fa-calendar" style={{ fontSize: '24px' }} />
                    </div>
                  </div>
                </div>
                <label className="col-2 col-form-label"> até </label>
                <div className="col-4">
                  <div className="row">
                    <div className="col-9">
                      <input
                        type="number"
                        min="1500"
                        className="form-control"
                        placeholder="Ano"
                        value={this.props.endYear}
                        onChange={this.onEndYearInputChange}
                      />
                    </div>
                    <div className="col-3" style={{ paddingTop: '7px' }}>
                      <i className="fa fa-calendar" style={{ fontSize: '24px' }} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col">
          <label className="col-form-label"> {this.props.amount} resultados enontrados </label>
        </div>
      </div>
    )
  }
}

export default SubHeader
