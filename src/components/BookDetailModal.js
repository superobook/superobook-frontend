//Modal component
import React, { Component } from 'react'

class BookDetailModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modalShow: '',
      display: 'none',
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.isOpen !== this.props.isOpen) {
      this.props.isOpen ? this.openModal() : this.closeModal()
    }
  }

  render() {
    return (
      <div
        className={'modal fade ' + this.props.modalShow}
        tabIndex="-1"
        role="dialog"
        aria-hidden="true"
        style={{ display: this.props.display }}
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header" style={{ backgroundColor: 'lightgray' }}>
              <h5 className="modal-title">
                {this.props.bookDetail && this.props.bookDetail.title}
              </h5>
            </div>
            <div className="modal-body">
              <li className="list-group-item">
                Autor: {this.props.bookDetail && this.props.bookDetail.author}
              </li>
              <li className="list-group-item">
                Editora: {this.props.bookDetail && this.props.bookDetail.publishingCompany}
              </li>
              <li className="list-group-item">
                Ano de publicação: {this.props.bookDetail && this.props.bookDetail.year}
              </li>
              <li className="list-group-item">
                Idioma: {this.props.bookDetail && this.props.bookDetail.language}
              </li>
              <li className="list-group-item">
                Peso: {this.props.bookDetail && this.props.bookDetail.weight} gramas
              </li>
              <li className="list-group-item">
                Comprimento: {this.props.bookDetail && this.props.bookDetail.dimension.length}{' '}
                milimetros
              </li>
              <li className="list-group-item">
                Largura: {this.props.bookDetail && this.props.bookDetail.dimension.width} milimetros
              </li>
              <li className="list-group-item">
                Altura: {this.props.bookDetail && this.props.bookDetail.dimension.height} milimetros
              </li>
            </div>
            <div className="modal-footer" style={{ backgroundColor: 'lightgray' }}>
              <button
                type="button"
                className="btn btn-info"
                data-dismiss="modal"
                onClick={this.props.onDetailButtonClose}
              >
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default BookDetailModal
