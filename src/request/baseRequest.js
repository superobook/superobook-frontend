import axios from 'axios'

export default class BaseRequest {
  constructor(configs) {
    this._axios = axios.create(configs)
  }

  setParams(path, params) {
    for (let i = 0; i < params.length; i++) {
      path = `${path}/${params[i]}`
    }
    return path
  }

  setQuery(path, query) {
    path = `${path}/?`
    for (let attr in query) {
      if (query.hasOwnProperty(attr)) {
        path = `${path}${attr}=${query[attr]}&`
      }
    }
    return path
  }

  urlHandler({ path, params, query }) {
    if (params) path = this.setParams(path, params)
    if (query) path = this.setQuery(path, query)
    return path
  }

  get({ path, headers, params, query }) {
    return this._axios.get(this.urlHandler({ path, params, query }), {
      headers,
    })
  }

  post({ path, headers, params, query, data }) {
    return this._axios.post(this.urlHandler({ params, path, query }), data, {
      headers,
    })
  }

  put({ path, headers, params, query, data }) {
    return this._axios.put(this.urlHandler({ params, path, query }), data, {
      headers,
    })
  }

  delete({ path, headers, params, query }) {
    return this._axios.delete(this.urlHandler({ path, params, query }), {
      headers,
    })
  }
}
