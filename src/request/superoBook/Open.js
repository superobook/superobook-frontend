import baseRequest from '../baseRequest.js'

const baseURL = require('../config').unipointURL

class Open extends baseRequest {
  constructor() {
    super({
      baseURL: baseURL,
    })
  }

  async getAmount(filters) {
    const query = this.getQuery(filters)
    try {
      let ret = await this.get({
        path: '/api/books/amount',
        query,
      })
      return ret.data
    } catch (e) {
      throw e.response || e
    }
  }

  async getBooks(page, amountPerPage, filters) {
    const query = this.getQuery(filters)
    try {
      let ret = await this.get({
        path: '/api/books',
        params: [page, amountPerPage],
        query,
      })
      return ret.data
    } catch (e) {
      throw e.response || e
    }
  }

  async getBook(id) {
    try {
      let ret = await this.get({
        path: '/api/book',
        params: [id],
      })
      return ret.data
    } catch (e) {
      throw e.response || e
    }
  }

  getQuery(filters) {
    let ret = {}
    for (let attr in filters) {
      if (filters.hasOwnProperty(attr) && !!filters[attr]) ret[attr] = filters[attr]
    }
    return ret
  }
}

export default new Open()
